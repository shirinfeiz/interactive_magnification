package stonybrook.interactivezoom;

import android.accessibilityservice.AccessibilityService;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityEventSource;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;

/**
 * Created by Shirin on 1/19/2018.
 */


public class MyAccessibilityService extends AccessibilityService implements SensorEventListener {
    private SensorManager mSensorManager;

    private Sensor mRotation;
    private Sensor mAccelSensor;
    private Sensor mMagnetSensor;



    private float[] mMagnetValues      = new float[3];
    private float[] mAccelValues       = new float[3];
    private float[] mOrientationValues = new float[3];
    private float[] mRotationMatrix    = new float[9];
    private float[] mOriginalOrientation = new float[3];
    private float[] mLastOrientationValues = new float[3];
    private float[] mposition = new float[3];
    private float[] mOriginalPosition = new float[3];
    private float diffVertical, diffHorizontal;

    private boolean touch;


    private boolean upKeyPressed;
    private boolean downKeyPressed;
    private boolean pressedUp;
    private boolean isStable;
    private boolean first;
    private float t1 = 0.3f,t2=0.05f; //two tresholds
    private float nextScale;



    //**********************************************************************************************
    //accessibility functions
    @Override
    public void onServiceConnected(){
        //setup initial values
        touch = false;
        pressedUp=false;
        upKeyPressed=false;
        downKeyPressed=false;
        first = true;

        isStable=true;

        nextScale = 0.5f;
        diffHorizontal=diffVertical=0;
        //get a sensor manager
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        //get sensors: rotation, acceleration and magnetometer
        mRotation = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        mAccelSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagnetSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        //register listener for sensors
        mSensorManager.registerListener(this, mRotation, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mAccelSensor, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mMagnetSensor, SensorManager.SENSOR_DELAY_GAME);

        //define a thread for zoom interactions
        new Thread(new Runnable() {

            @Override
            public void run() {
                while(true){
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                    //Log.d("notify", -mOrientationValues[1]+"\t"+moriginalOrientation[1]+"\t"+mOrientationValues[2]+"\t"+moriginalOrientation[2]);
                    float vertical = -mOrientationValues[1]+mOriginalOrientation[1];
                    float horizontal = mOrientationValues[2]-mOriginalOrientation[2];
                    doMove(vertical,horizontal);

                    float pos1 = mOriginalPosition[1]-mAccelValues[1];
                    float pos2 = mOriginalPosition[2]-mAccelValues[2];
                    doMagnify(pos1,pos2);
                    Log.d("notify", vertical+"\t"+horizontal);
                    //Log.d("notify", mAccelValues[0]+"\t"+mAccelValues[1]+"\t"+mAccelValues[2]);//secound or magneto meter 3rd
                }
            }
        }).start();

    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event){
        //WE NO LONGER USE ACCESSIBILITY EVENTS! for overriding issues!
        //............................................................


        //Log.d("event", "onAccessibilityEvent: "+event.toString());
        //move when hold
        /*if(event.getEventType()==AccessibilityEvent.TYPE_TOUCH_INTERACTION_START) {
            touch = true;
        }
        if(event.getEventType()==AccessibilityEvent.TYPE_TOUCH_INTERACTION_END) {

            touch = false;
        }*/

        //move when clicked
        /*if(event.getEventType()==AccessibilityEvent.TYPE_VIEW_CLICKED) {
            doMove(-mOrientationValues[1],mOrientationValues[2]);
        }*/
/*
        //set default magnification
        if(event.getEventType()==AccessibilityEvent.TYPE_WINDOWS_CHANGED){
            final MagnificationController controller = getMagnificationController();
            boolean res = controller.setScale(0.5f, true );
        }
*/
            /*Log.d("notify", "onAccessibilityEvent: " +
                    String.valueOf(rotation[0]) + " "
                    + String.valueOf(rotation[0]) + " "
                    + String.valueOf(rotation[0]));
            magnify(true);*/

    }
    @Override
    public void onInterrupt(){
        Log.d("notify", "onInterrupt: ");

    }

    public void onUnbind(){
        Log.d("notify", "onUnbind: ");
        //unregister sensor listeners
        mSensorManager.unregisterListener(this, mRotation);
        mSensorManager.unregisterListener(this, mAccelSensor);
        mSensorManager.unregisterListener(this, mMagnetSensor);

    }


    //sensor reading
    protected void onResume() {
        Log.d("notify", "onResume: ");
        mSensorManager.registerListener(this, mRotation, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mAccelSensor, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mMagnetSensor, SensorManager.SENSOR_DELAY_GAME);
    }

    protected void onPause() {
        Log.d("notify", "onPause: ");
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.d("notify", "onAccuracyChanged: ");
        first =true;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        //actively measure orientation based on changes on sensor data!

            switch (event.sensor.getType()) {
                case Sensor.TYPE_ACCELEROMETER:
                    System.arraycopy(event.values, 0, mAccelValues, 0, 3);
                    break;

                case Sensor.TYPE_MAGNETIC_FIELD:
                    System.arraycopy(event.values, 0, mMagnetValues, 0, 3);
                    break;
            }
            SensorManager.getRotationMatrix(mRotationMatrix, null, mAccelValues, mMagnetValues);
            System.arraycopy(mOrientationValues, 0, mLastOrientationValues, 0, 3);
            SensorManager.getOrientation(mRotationMatrix, mOrientationValues);
            if(mOriginalOrientation[1]==0.0f && mOriginalOrientation[2]==0.0f)
            {
                System.arraycopy(mOrientationValues, 0, mOriginalOrientation, 0, 3);
                System.arraycopy(mAccelValues, 0, mOriginalPosition, 0, 3);
                first = false;


            }





    }

    //**********************************************************************************************
    //key events
    @Override
    public boolean onKeyEvent(KeyEvent event) {

        int action = event.getAction();
        int keyCode = event.getKeyCode();

        if (action == KeyEvent.ACTION_DOWN) {
            if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
                upKeyPressed = true;
                magnify(true);
            } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
                downKeyPressed = true;
                magnify(false);
            }
        }
        else if (action == KeyEvent.ACTION_UP) {
            if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {    upKeyPressed = false;   }
            else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) { downKeyPressed = false; }
        }
        /*else {
        //super.onKeyEvent(event);
        always comes here
        super.event()
            Log.d("notify", "onKeyEvent: no key event");
        }*/

        return true;
    }


    //**********************************************************************************************

    //**********************************************************************************************
    //controlled magnification
    private  boolean doMove(float vertical , float horizontal){
        if(isStable){
            if (Math.abs(horizontal/3.14f) > t1) {
                horizontal = horizontal / 3.14f;
                isStable = false;
            }
            else
                horizontal = 0f;

            if (Math.abs(vertical/3.14f) > t1) {
                vertical = vertical / 3.14f;
                isStable = false;
            }
            else
                vertical = 0f;
        }else {
            if (Math.abs(horizontal/3.14f) > t2)
                horizontal= horizontal/3.14f;
            else {
                horizontal = 0f;
                isStable = true;
            }

            if (Math.abs(vertical/3.14f) > t2)
                vertical= vertical/3.14f;
            else {
                isStable=true;
                vertical = 0f;
            }

        }

        /*if (Math.abs(horizontal/3.14f) > 0.005f)
            horizontal= horizontal/3.14f;
        else
            horizontal = 0f;

        if (Math.abs(vertical/3.14f) > 0.005f)
            vertical= vertical/3.14f;
        else
            vertical = 0f;

*/
        final float speed = 0.3f;

        final MagnificationController controller = getMagnificationController();
        final DisplayMetrics metrics = getResources().getDisplayMetrics();
        float stepX = metrics.widthPixels*speed*horizontal;
        float stepY = metrics.heightPixels*speed*vertical;

        if(stepX==0f && stepY==0f)
            return false;
        //Log.d("Move", stepX+"\t"+stepY);
        controller.setCenter(controller.getCenterX()+stepX, controller.getCenterY()+stepY, true);
        return false;
    }

    private void doMagnify(float pos1 , float pos2){
        if(pos1 > 0.3 ) {
            magnify(false);
        }
        else
            if(pos1 <-0.3)
                magnify(true);

    }
    private boolean magnify(boolean upscale) {
        //upscale == true : zoom in
        //upscale == false : zoom out
        final MagnificationController controller = getMagnificationController();
        final float currScale = controller.getScale();
        final float increment = upscale ? 0.1f : -0.1f;
        nextScale = Math.max(1f, Math.min(10f, currScale + increment));
        if (nextScale == currScale) {
            return false;
        }
        final DisplayMetrics metrics = getResources().getDisplayMetrics();
        boolean res = controller.setScale(nextScale, true );


        return true;
    }

}
